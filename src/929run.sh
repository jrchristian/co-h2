#!/bin/bash

#-----------------------------------------------------------
# run post processing on some of the snapshots in 
# /data/desika/gadgetruns/sbw_tests/mw_18_6_hr_hightimeres/
# 
# run using qsub -I -l nodes=n15 on fock
#-----------------------------------------------------------

for i in /desika/gadgetruns/SIGS/c6e_hdf5/*.hdf5; do
        # strip the directory and file extension
        file="${i##*/}"
        directory="/students/jchristi/data/gadgetpost/c6e/"
        post="_post"
        tag=".hdf5"
        # reconstruct a new filename
        filename="$directory${file%.hdf5}$post$tag"
        if [ ! -f $filename ]; then
                python gadget_cloud.py "${i}"
        else
                echo "exists $filename"
        fi
        done
