#!/bin/sh

#---------
# Bash script to run DESPOTIC in parrallel on .hdf5 post files
#---------

# -j+0 tells gnu parallel to run as many jobs as cores + 0 extra
# --delay 30 delays the spawning of a new process by 30s. i implemented this to prevent
# a really slow start with tons of IO happening at once.

#ls /students/jchristi/data/gadgetpost/sbw_mergers/mw_e_hr/*.hdf5 | time parallel -j-4 --joblog logfile --delay 30 --no-notice python cosleds.py
ls /students/jchristi/data/gadgetpost/c6e/*.hdf5 | time parallel -j-4 --joblog logfile --delay 30 --no-notice python cosleds.py
