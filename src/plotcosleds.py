# Collect the SFR data -- this is all temporary
from yt.mods import *
import yt.units
from yt.geometry.oct_container import OctreeContainer
from yt.geometry.selection_routines import AlwaysSelector
import numpy as np
import os
files = os.listdir("/desika/gadgetruns/SIGS/c6e_hdf5")
files.sort()
SFR = np.array([])
globalSFR = np.array([])
weightedSigmaSFR = np.array([])
""" Analyze the higest density grid from gadget snapshot
All of the IO and variable declarations go here
"""
bbox = [[-7000,7000],[-7000,7000],[-7000,7000]] 
unit_base = {"UnitLength_in_cm"         : 3.08568e+21,
            "UnitMass_in_g"            :   1.989e+43,
                        "UnitVelocity_in_cm_per_s" :      100000}

for f in files:
    try:
        if f.split('.')[1] == 'hdf5':
            fname = str(f.split('_')[1].split('.')[0]).zfill(5) + '.png'
            ds = load("/desika/gadgetruns/SIGS/c6e_hdf5/"+f, unit_base = unit_base, bounding_box = bbox,over_refine_factor=0)
            ds.index
            ad = ds.all_data()

            always = always = AlwaysSelector(None)
            fc1 = ds.index.oct_handler.fcoords(always)# coordinates in kpc
            fw1 = ds.index.oct_handler.fwidth(always) # width of cell in kpc
            gridCenters = fc1
            gridWidths = YTArray(fw1, "kpc")

            sfr = ad["PartType0", "StarFormationRate"]
            globalSFR = np.append(globalSFR, sfr.sum())
            sigmaSFR = sfr / np.power(gridWidths.in_cgs()[:,0],2)
            # just like weighting a density by mass
            weightedSigmaSFR = np.append(weightedSigmaSFR, np.average(sigmaSFR,weights=sfr))
    except: IndexError
##################################################################
# THE ORIGINAL SCRIPT BELOW VVVVV
##################################################################

#------------------------
# Read in the .npy files and do some analysis on the 
# prelimary XCO and SLED relationship
#------------------------

from progressbar import ProgressBar
import numpy as np
import matplotlib.pyplot as plt
import os
import glob

#files = glob.glob('/students/jchristi/data/gadgetpost/sbw_tests/mw_18_6_hr_hightimeres/*.npy')
#files = glob.glob('/students/jchristi/data/gadgetpost/sbw_mergers/mw_e_hr/*.npy')
files = glob.glob('/students/jchristi/data/gadgetpost/c6e/*_post_XCO_4_2.npy')
files.sort()

totXCO = np.array([])
lumWeightedXCO = np.array([])
areaWeightedXCO = np.array([])
jpeak = np.array([])
SLEDs = []
weightedSLEDs = []

pbar, ct = ProgressBar(maxval=len(files)), 0
pbar.start()
for i in range(len(files)):
	x, sled, z, p, q, r = np.load(files[i])
        totXCO = np.append(totXCO, x[0])
        lumWeightedXCO = np.append(lumWeightedXCO, x[1])
        areaWeightedXCO = np.append(areaWeightedXCO, x[3])
	#jpeak = np.append(jpeak, np.argmax(y))
    #SLEDs.append(z)
    #DEBUG
        #sled = np.sum(z,axis=0)
        #sled = sled/sled[0]
        SLEDs.append(sled)
    #END DEBUG
	#weightedSLEDs.append(y) # luminosity weighted SLEDs
    # update the progress bar
	pbar.update(ct)
	ct += 1

#--------------------
# generate the plots
#--------------------

# Four axes, returned as a 2-d array

f, axarr = plt.subplots(2, 2)
#f.set_title('Methods of computing $X_{CO}$ for galaxy')
axarr[0, 0].plot(globalSFR,'b',lw=2)
axarr[0, 0].set_title('Global $SFR$')
axarr[0, 1].plot(totXCO, 'r',lw=2)
axarr[0, 1].set_title('$\Sigma nH / \Sigma W_{CO}$')
axarr[1, 0].plot(areaWeightedXCO, 'g',lw=2)
axarr[1, 0].set_title('$(\Sigma nH \cdot A)/ (\Sigma W_{CO} \cdot A)$')
axarr[1, 1].plot(lumWeightedXCO, 'm',lw=2)
axarr[1, 1].set_title('LCO Weighted')
# hide the x-ticks on the top two plots
plt.setp([a.get_xticklabels() for a in axarr[0, :]], visible=False)
plt.savefig('/students/jchristi/co-h2/images/c6e_4panel_xco.png')
plt.clf()


# plot the SLEDs
# Plot the resulting SLEDs on a y axis which has a log scale
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set_yscale('log')

plt.ylabel("$I$/$I_{1-0}$")
skip=1
ax.set_xticks(range(0,40,skip))
ticklabels=["$J={{{0}}}-{{{1}}}$".format(skip*j+1,skip*j) \
                for j in range(40)]
ax.set_xticklabels(ticklabels)
plt.title("CO SLEDs c6e prelim")
plt.xlim([0,8])
plt.ylim([1,100])
line, = ax.plot(np.power(np.arange(1,41),2),'r--',lw=3)
for i in xrange(len(SLEDs)):
    # try plotting the SLED weighted by all 40 line luminosities
    # DEBUG
    if i % 15 == 0:
    # DEBUG END
        line, = ax.plot(SLEDs[i], lw=2,label=str(i))
    #line, = ax.plot(SLEDs[i][0], lw=2)
# DEBUG    
plt.legend()
plt.savefig("/homes/jchristi/Dropbox/Thesis/Images/RJ_test.png")
#plt.savefig("/students/jchristi/co-h2/images/c6e_test_sleds.png")
# END DEBUG
plt.clf()


#--------------------------------------------------
# plot JPeak vs. XCO
#--------------------------------------------------
#plt.plot(lumWeightedXCO,jpeak)
#plt.savefig("/students/jchristi/co-h2/images/c6e_jpeak_vs_xco.png")

#--------------------------------------------------
# plot XCO vs. a couple of different line ratios
#--------------------------------------------------
