#--------------------------------------------
# A simple script to save a python dictionary
# to an hdf5 file
#--------------------------------------------
# Desika,
# I am not sure if this will work for any 
# bigger datasets you might have. Just not 
# sure how much you can actually cram into
# a python dictionary at a reasonable speed
# this will do the trick for our gadget 
# post processing though!
#
# Joel
#--------------------------------------------
# requires h5py
#
# <jchristi@haverford.edu>
#--------------------------------------------

from h5py import File

def hdf5Save(filename, data):
	""" save a python dictionary to hdf5:
	The input to this function must be a
	python dictionary with data labels as
	keys and arrays or similar data objects
	as keys
	-------------------------------------
	>>> myDict = {'x':[1,2,3,4]}
	>>> hdf5Save('junk.hdf5', myDict)
	"""
	f = File(filename, mode="w")	# create a file
	
	dsets = []			# something to hold dsets
	for i in data:
		dsets.append(f.create_dataset(i, data=data[i]))

	f.close()			# close the file

def hdf5Load(filename):
	""" read a hdf5 file saved with the
	hdf5Save function and write the conents
	to a pythpn dictionary.
	"""
	f = File(filename, mode='r')

	data = {}			# empty dict for populating
	fieldList = []
	f.visit(fieldList.append)	# populate the field list
	for i in fieldList:
		entry = f[i].value	# access the data from hdf5
		name = str(i)		# unicode -> string conv.
		data[name] = entry	# make the assingment to dict

	return data
