""" script for gadget post processing
Joel Christian
<jchristi@haverford.edu>
June 2014
data from http://ajax.as.arizona.edu/~desika/snapshots/snapshot_017.hdf5
"""
from yt.mods import *
from yt.geometry.oct_container import OctreeContainer
from yt.geometry.selection_routines import AlwaysSelector
from functools import partial
import multiprocessing as mp
import numpy as np
import numpy.ma as ma
import pdb,sys
import matplotlib.pyplot as plt
import time
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
from fastgadget import gaussQuad, eDist
from hdf5Writer import hdf5Save, hdf5Load

def genChunks(alist, nchunks):
	""" Generate chunks of a list for pooled mapping of processes
	Input:
	-- alist: the list to be chunked
	-- nchunks: the number of chunks to be created
	Output:
	-- listOfChunks: alist broken into nchunks
	"""
	chunkStartIndex = [0] # first chunk starts with element 0
	deltaChunkIndex = int(len(alist) / nchunks)
	for n in range(1, nchunks):
		chunkStartIndex.append(chunkStartIndex[n-1]+deltaChunkIndex)
	listOfChunks = []
	for n in range(nchunks):
		alistChunk = alist[chunkStartIndex[n]:chunkStartIndex[n]+deltaChunkIndex]
		if n == nchunks-1:
			alistChunk = alist[chunkStartIndex[n]:]
		listOfChunks.append(alistChunk)
	return listOfChunks

def fh2(sigmaCloud, Zprime):
	""" Compute the H2 molecular gas fraction.
	Inputs (YT or Numpy arrays):
	-- sigmaCloud = cloud surface density
	-- Zprime = metallicity divided by solar metallicity 
	Outputs:
	-- molecular gas fraction 
	** 1 implies entirely H2, 0 implies entirely H1
        """
	chi = (0.76 * (1.0 + 3.1 * (Zprime ** 0.365)))
        tau_c = ((0.066 * sigmaCloud) * Zprime)
        s = (np.log(1.0 + 0.6 * chi + 0.01 * np.power(chi,2))/(0.6 * tau_c))
        s = np.array(s)
	# use np.less to create masks rather than iterative comparison
        lomask = np.less(s, 2)
        himask = np.logical_not(lomask)
        fco = s
        fco[lomask] = 1.0 - (.75) * (s[lomask]/(1.0 + 0.25 * s[lomask]))
        fco[himask] = 0.0	# mask high values to zero and low vals to formula
        return fco

def fco(fh2, Zprime, SFR, nH):
	""" Compute the CO molecular gas fraction using numpy arrays
	Inputs:
	-- fh2, the hydrogen molecular gas fraction
	-- Zprime,
	-- SFR, the star formation rate
	-- colDen, the area density (column density) in cgs units
	Outputs:
	-- molecular gas fraction
	
	See <http://en.wikipedia.org/wiki/Extinction_%28astronomy%29> for more
	on units etc...
	"""
	Zprime = np.array(Zprime)
	nH = np.array(nH)		# numpy array conversion is important here
	gprime = np.float(SFR.sum()/2.0) 	# divide SFR by MW SFR
	Av = nH / 1.8e21
	fco = fh2*np.exp(-4*(0.53-0.045*np.log(gprime/nH)-0.097*np.log(Zprime))/Av)
	# mask off any nan entries. set these equal to 0.0'
	mask = np.isnan(fco)
	fco[mask] = 0.0
	return fco

def sigmaNT(chunk):
	""" Compute the non-thermal velocity dispersion in units of cm/s 
	Input:
	-- chunk: a chunk of indices which indicate which cell in the snapshot is 
	about to be analyzed
	Output:
	-- sigma: the non-thermal velocity dispersion in cm/s of the cells indicated
	by the indices stored in chunk
	"""
	sigNT = np.array([])
	for index in chunk:
		rad = gridWidths[index][0] * 1.5 # radius 1.5 x the width of the grid size
		distances = eDist(partCoord, gridCenters[index])
		inSphere = np.where(distances <= rad)[0]
		# generate array of vx, vy, vz for the particles that fall within the sphere
		velInSphere = partVel[inSphere]
		# add the velocities in quadrature and then take their standard deviation
		gauss = gaussQuad(velInSphere)
		sigma = np.std(gauss)
		if inSphere.size == 0: # catch the case where there are no particles
			sigma = 0
		sigNT = np.append(sigNT, sigma)
	return sigNT

#-------------------------------------------------
# YT Gadget IO component
# THE SCRIPT ITSELF. FEED THIS A COMMAND LINE ARGUMENT WITH THE FILENANE.


filename = sys.argv[1]
#filename = "snapshot_017.hdf5"
nprocs = 6#12
	
""" Analyze the higest density grid from gadget snapshot
All of the IO and variable declarations go here
"""
bbox = [[-7000,7000],[-7000,7000],[-7000,7000]] 
unit_base = {"UnitLength_in_cm"         : 3.08568e+21,
            "UnitMass_in_g"            :   1.989e+43,
            "UnitVelocity_in_cm_per_s" :      100000}
	
ds = load(filename, unit_base = unit_base, bounding_box = bbox,over_refine_factor=0)
ds.index
ad = ds.all_data()

""" Smooth the gadget data onto an octree and find the coorindates and cell \
dimensions. TRY TO FIND GEOMETRY SOURCE IN YT SOURCE.
"""
print "Saving ",filename ," to an octree..."
saved = ds.index.oct_handler.save_octree()
refined = saved["octree"]
global density, mass, metallicity, partVel, particleVel, partCoord
global SFR, Zsolar, gridCenters, gridWidths, fw1, fc1
global Z, Zprime, cellsnH, cellsSigmaCloud#, gasFrac, coGasFrac
density = ad["deposit", "PartType0_smoothed_density"]
mass = ad["deposit", "PartType0_smoothed_particle_mass"]
metallicity = ad["deposit", "PartType0_smoothed_metallicity"]
partVel = ad["PartType0","Velocities"].in_cgs()
partCoord = ad["PartType0","Coordinates"]
SFR = ad["PartType0", "StarFormationRate"]
Zsolar = YTQuantity(0.02, "dimensionless")
always = always = AlwaysSelector(None)
fc1 = ds.index.oct_handler.fcoords(always)	# coordinates in kpc
fw1 = ds.index.oct_handler.fwidth(always) 	# width of cell in kpc
gridCenters = fc1 # set this as a global. this is a hack to get pool.map to work
gridWidths = YTArray(fw1, "kpc")
#-------------------------------------------------




if __name__ == "__main__":

	print "quick array calculations..."
	# Only run sigmaNT and DESPOTIC on the cells which do not have
	# Compute some of these quantities ONCE
	cellsnH = density/YTQuantity(1.67e-24,"g")
	Z = metallicity
	Zprime = metallicity/Zsolar
	
	# SigmaCloud -> max(SigmaCell, 85 MSolar pc^2)
	cellsSigmaCloud = density.in_cgs() * gridWidths.in_cgs()[:,0]
	SigmaMax = YTQuantity(85.0, "Msun/pc**2").in_cgs()
	cellsSigmaCloud = np.maximum(cellsSigmaCloud, SigmaMax)
	
	# convert units for the fh2 calculation
	cellsSigmaCloudUnits = cellsSigmaCloud.convert_to_units("Msun/pc**2")
	cellsGasFrac = fh2(cellsSigmaCloudUnits, Zprime)
	cellsCOGasFrac = fco(cellsGasFrac, Zprime, SFR, cellsnH)
	cellsSigmaCloud = cellsSigmaCloud.in_cgs()	# need explicit declaration of this
	print "mask of fH2 > 0.0"
	# compute sigmaNT for the cells with fH2 > 0.0
	mask = np.greater(cellsGasFrac, 0.0)
	print "computing sigmaNT: the multiprocessing component..."
	p = mp.Pool(nprocs)
	indices = np.arange(density.size)[mask]

	try: 
		sys.argv[2] == '-s'
		single = True
	except IndexError:
		single = False
	if single == False:
		chunks = genChunks(indices, nprocs) # generate chunks for multiprocessing
		results = p.map(sigmaNT, chunks)
		p.close()
		p.join()
		cellsSigmaNT = np.concatenate([results[i] for i in xrange(len(results))])

	if single == True:
		results = map(sigmaNT, indices)

	gridWidths = gridWidths.in_cgs() # save in cm 
	
	# save to file
	print "save to file..."
	fName = "/students/jchristi/data/gadgetpost/c6e/"+filename.split("/")[-1].split(".")[0]+"_post.hdf5"
	postData = {
	"Z":Z,
	"Zprime":Zprime,
	"SigmaCloud":cellsSigmaCloud,
	"GasFrac":cellsGasFrac,
	"nH":cellsnH,
	"COGasFrac":cellsCOGasFrac,
	"SFR":SFR,
	"SigmaNT":cellsSigmaNT,
	"mask":mask,
	"mass":mass,
	"density":density,
	"gridWidths":gridWidths
	}
	hdf5Save(fName, postData)
