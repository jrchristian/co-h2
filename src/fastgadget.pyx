""" fastgadget.pyx
Joel Christian
<jchristi@haverford.edu>
June 2014
Cython routines to speed up gadget post-processing
"""

cimport cython
from libc.math cimport sqrt, pow
from numpy cimport ndarray
from numpy import empty

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef gaussQuad(ndarray[double,ndim=2] a):
    """ Cython gaussian quadrature function:
    Inputs:
    -- a is a 2D vector of x, y and z velocities
    Outputs:
    -- an array with the sum in quadrature of vx, vy & vz
    """
    cdef:
        int len_a = a.shape[0]
        int wid_a = 3
        ndarray[double,ndim=1] y = empty(len_a)
        Py_ssize_t p, q
        float m

    for p in xrange(len_a):
        m = 0.0
        for q in xrange(wid_a):
            m += pow(a[p,q],2)
        y[p] = sqrt(m)

    return y


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef eDist(ndarray[double,ndim=2] a, ndarray[double,ndim=1] b):
    """ Cython euclidian distance function:
    Inputs:
    -- a is a 2D vector of x, y and z coordinates
    -- b is a 1D array fiducial point from which we want to calculate
        the distance
    Outputs:
    -- an array with the euclidian distance from each point to the
        fiducial point.
    """
    cdef:
        int len_a = a.size / 3
        int wid_a = 3
        int len_b = b.size
        ndarray[double,ndim=2] x = empty([len_a, wid_a])#, dtype="float64")
        ndarray[double,ndim=1] y = empty(len_a)#, dtype="float64")
        Py_ssize_t i, j, p, q
        #int i, j, p, q 
        float m

    for i in xrange(len_a):
        for j in xrange(len_b):
            x[i,j] = a[i,j] - b[j]

            
    for p in xrange(len_a):
        m = 0.0
        for q in xrange(wid_a):
            m += pow(x[p,q],2)
        y[p] = sqrt(m)

    return y