#--------------------------------------------
# CO SLED script
# Joel Christian
# <jchristi@haverford.edu>
#-------------------------------------------- 
# Run this script on an hdf5 file with 
# derived fields from GADGET snapashot
#--------------------------------------------
import numpy as np
import scipy.constants
from despotic import cloud
from yt.mods import *
from progressbar import ProgressBar
from time import time
from sys import argv, exit
from hdf5Writer import hdf5Save, hdf5Load
from os.path import isfile
# Constants
kb = 1.3806488e-16 # erg/K Boltzmann's Constant in cgs
c_light = scipy.constants.c * 100 # explicit conversion to cgs

# File IO
filename = argv[1]
savename = filename.split(".")[0] + "_XCO_4_3.npy"
if isfile(savename) == True: 
    # exit this code if despotic has already been run
    print savename, 'exists...'
    exit()
else:
    print savename
data = hdf5Load(filename)

# unpack the mask used for sigmaNT post processing
mask = data['mask']
indices = np.arange(data['density'].size)[mask]

# note this calculation here... 3.3e-24 g/H2 molecule
colDensity = ((data['SigmaCloud']*data['GasFrac'])/(3.3e-24))
cellsnH = data['density']/YTQuantity(1.67e-24,"g")
HperCell = np.array(data['nH']*np.power(data['gridWidths'][0:,0],3))
globSFR = data['SFR']#.sum()
mass = data['mass']
mH = 1.677e-27 # mass of a hydrogen nuclei
gridWidths = data['gridWidths'] # gw in Kpc

SLEDs = []
XCO = np.array([])
luminosities = []
intensities = np.array([])
lineIntTB = np.zeros(40)
cloudIntTB = np.array([])
cloudColDen = colDensity[mask]
areas = np.power((gridWidths[mask]*1000)[:,0],2)

#-------------------------------------------- 
# Loop over every grid cell with H2 in the
# entire simulation snapshot
#--------------------------------------------
t1 = time()
pbar, ct = ProgressBar(maxval=indices.size), 0
pbar.start()
for index in indices:
    gmc = cloud()	# initialize a cloud
    #gmc.dVdr = 	# radial velocity gradient [s^-1]
    gmc.Tg = 10.0 	# the gas temp [K]
    gmc.Td = 30.0 	# the dust temp [K]
    
    gmc.nH = data['nH'][index] # number density of H nuclei [cm^-3]
    gmc.colDen = colDensity[index] # center-to-edge column density of H nuclei [cm^-2]
    gmc.sigmaNT = data['SigmaNT'][ct] # the non thermal velocity dispersion [cm s^-1]

    # Dust properties
    gmc.dust.alphaGD = 3.2e-34    # Dust-gas coupling coeff [erg cm^3 K^-3/2]
    gmc.dust.sigmaD10 = 2.0e-26   # Cross section to 10K thermal radiation [cm^2 H^-1]
    gmc.dust.sigmaDPE = 1.0e-21   # Cross section to 8-13.6 eV photons [cm^2 H^-1]
    gmc.dust.sigmaDISRF = 3.0e-22 # Cross sect to ISRF phots[cm^2 H^-1]
    gmc.dust.Zdust = 1.0          # Dust abundance relative to solar
    gmc.dust.betaDust = 2.0       # Dust spectral index

    # set chemical composition
    gmc.xH2 = data['GasFrac'][index] 	# H2 abundance per H nucleus
    gmc.xCO = data['COGasFrac'][index]	# CO abundance per H nucleus

    # not sure about these two. i pulled them from despotic manual
    gmc.comp.xoH2 = 0.1 	# ortho H2 abundance
    gmc.comp.xpH2 = 0.4 	# para H2 abundance
    gmc.comp.xHe = 0.1 	# He abundance per H nucleus

    # set radiation field
    #gmc.rad = # the radiation field impinging on the cloud
    gmc.rad.TCMB = 2.73 	# CMB temperature [K]
    gmc.rad.TradDust = 0.0 	# dust-trapped IR rad field temp [K]
    gmc.rad.chi = globSFR / 2.0 # ISRF strength, relative to solar neighborhood
    gmc.rad.ionRate = (globSFR/2.0)*(10**-16) # Cosmic x-ray ionization rate [s^-1 H^-1]

    #------------------------
    # add emitters that we would like to track along with their 	
    # abundance relative to H
    #------------------------
    gmc.addEmitter('co', 1.0e-4) # CO abundance
    # compute the luminosity of the CO lines in the cloud           
    gmclines = gmc.lineLum('co') 	# calculate line luminosities here
    #lines = np.append(lines, gmclines)
    XCO = np.append(XCO, gmc.colDen/gmclines[0]['intTB'])
    gmcIntensity = np.array([line['intIntensity'] for line in gmclines])
    gmcFreq = np.array([line['freq'] for line in gmclines]) #in Hz NOT GHz
    intensities = np.append(intensities, gmcIntensity[0])
    gmcBrightnessTemp = np.array([line['intTB'] for line in gmclines])
    # strange calculation for weighting -- might scrap these two lines
    gmcTBint = np.matrix([line['intTB'] for line in gmclines])
    lineIntTB = np.vstack((lineIntTB, gmcTBint))
    
    #------------------------
    # single cell luminosity of the line per H nucleus 
    # (there is one entry for each of the 40 levels)
    #------------------------
    lumPerH = [line['lumPerH'] for line in gmclines] # [erg s^-1 H^-1]
    # multiply by Hydrogen atoms in the cells
    lum = np.multiply(lumPerH, HperCell[index]) # multiply by H per cell
    lumArea = np.multiply(lum, np.power((gridWidths[index][0]*1000),2))
    luminosities.append(lumArea)
    # BEGIN DEBUG RAYLEIGH JEANS TEST
    #sled = (gmcIntensity/gmcFreq)	# divide out the frequencies
    #sled = np.divide(np.multiply(gmcIntensity, np.power(c_light,2)),kb * np.power(gmcFreq,2))
    sled = kb * gmcBrightnessTemp * np.divide(2 * np.power(gmcFreq, 2), np.power(c_light, 2))
    # gmcIntensity = frequency integrated intensity of the line in ergs cm^-2 s^-1 Hz^-1 
    # boltzmann erg K^-1
    # top ergs cm^-2 * cm^2/s^2
    # bottom  ergs K^-1 * Hz^2
    # END DEBUG RAYLEIGH JEANS TEST

    #------------------------	
    # Mask negative values to avoid warnings. Negative values are 
    # obtained for some of the very high J states due simply to 
    # roundoff error. 	
    #------------------------	
    sled[sled <= 0] = 1e-50
    SLEDs.append(sled)
    cloudIntTB = np.append(cloudIntTB, gmclines[0]['intTB'])
        
    # update the progress bar
    pbar.update(ct)
    ct += 1

#------------------------
# Sum all of the SLEDs for this galaxy and normalize by the 1-0 line
#------------------------
SLED = np.sum(SLEDs,axis=0)
SLED = SLED/SLED[0]

lineIntTB = np.delete(lineIntTB, 0, axis=0) 	# peel off the row of zeros on the lineIntTB array
totalIntTB = np.sum(lineIntTB,axis=0) # sum up the intTB for every unique line
maskedColDen = colDensity[mask]
totColDen = np.sum(maskedColDen)
pbar.finish()
t2 = time()

print "completed CO SLED for %s in %f seconds" % (filename, t2-t1)

#------------------------
# Try a couple of different calculations of XCO
#------------------------
# total colden/total intTB line 1-0
totXCO = (totColDen/totalIntTB[0,0])

# luminosity weighted average XCO
# trying something new here -- use CO 1-0 L_CO
LCO = [i[0] for i in luminosities]
LCO = np.array(LCO)
lumWeightedXCO = np.average(XCO,axis=0,weights=LCO)
# intensity weighted XCO
intWeightedXCO = np.average(XCO,weights=intensities)
# XCO -> sum(nH * dA)/sum(wco * dA)
areaWeightedXCO = np.divide(np.dot(cloudColDen,areas),np.dot(cloudIntTB,areas))

# an array with the other values of interest
XCO_out = np.array([totXCO, lumWeightedXCO, intWeightedXCO, areaWeightedXCO])
np.save(savename, (XCO_out,SLED,SLEDs,cloudColDen,cloudIntTB,areas))
