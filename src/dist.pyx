from __future__ import division
import numpy as np
# "cimport" is used to import special compile-time information
# about the numpy module (this is stored in a file numpy.pxd which is
# currently part of the Cython distribution).
cimport numpy as np
# We now need to fix a datatype for our arrays. I've used the variable
# DTYPE for this, which is assigned to the usual NumPy runtime
# type info object.
DTYPE = np.int
# "ctypedef" assigns a corresponding compile-time type to DTYPE_t. For
# every type in the numpy module there's a corresponding compile-time
# type with a _t-suffix.
ctypedef np.int_t DTYPE_t

cdef np.ndarray[float,ndim=1] norm(np.ndarray[float,ndim=2] a, np.ndarray[float,ndim=1] b):
	""" Cython euclidian distance function
	"""
	cdef int len_of_a = a.shape[0] # rows
	cdef int wid_of_a = a.shape[1] # col
	cdef int len_of_b = b.shape[0]
	cdef np.ndarray[float,ndim=2] x = np.zeros([len_of_a, wid_of_a], dtype=float)
	cdef np.ndarray[float,ndim=1] y = np.zeros(len_of_a, dtype=float)
	cdef int i, j, p, q 
	cdef float m
	for i in range(len_of_a):
		#for j in range(len_of_b):
		#	x[i][j] = a[i][j] - b[j]
		x[i] -= b

	for p in range(len_of_a):
		#m = 0.
		m = np.sum(x[p]**2)
		#for q in range(len_of_b):
		#	m = m + np.power(x[p][q], 2)
		y[p] = np.sqrt(m)

	return y